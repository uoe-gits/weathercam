const imagePath = 'frames';
const imgAlt = "View from JCMB";
const totalFrames = 60;

var timer = null;
var frameNumber = 0;
var current = 0;
var speed = 200;
var delay = 0;

// append cache breaking parameter to images to ensure we always get the latest version
var images = new Array(totalFrames);
for (let i = 0; i < images.length; i++) {
    images[i] = imagePath+'/'+i.toString().padStart(3, '0')+".jpg?t=" + new Date().getTime();
}

/* update the index every minute */
setInterval(function() {
    request = new XMLHttpRequest;
    request.open('GET', imagePath+"/idx.json" , true);

    request.onload = function() {
	if (request.status >= 200 && request.status < 400){
	    // Success!
	    weather = JSON.parse(request.responseText);
	    current = weather.idx;
	    frameNumber = weather.idx;
    
	    // update image list
	    images[frameNumber] = imagePath+'/'+frameNumber.toString().padStart(3, '0')+".jpg?t=" + new Date().getTime();
	    if (timer === null) {
		document.getElementById("weathercam").src = images[frameNumber];
	    }

	} else {
	    // We reached our target server, but it returned an error
	    
	}
    };

    request.onerror = function() {
	// There was a connection error of some sort
    };

    request.send();    
}, 60 * 1000);

function weather_animate(go) {
    if(go || timer === null) {
        /* while frameNumber less than totalFrames increment else back to 0 */
        var s = speed;
        frameNumber += 1;
	// short pause at the current frame
        if (frameNumber == current ) {
                s = speed * 5 + 100;
        }
        if (frameNumber >= totalFrames){
                frameNumber = 0;
        }
        document.getElementById("weathercam").src = images[frameNumber];
        document.getElementById("weathercam_start").innerHTML = "Stop Time Lapse";
        timer=setTimeout("weather_animate(true)", s);
    } else {
        clearTimeout(timer);
        timer = null;
        document.getElementById("weathercam_start").innerHTML = "Start Time Lapse";
    }
}

function weather_faster() {
   speed -= 50;
   if (speed <= 0) { speed = 1 };
}

function weather_slower() {
   speed += 50;
}

$(document).ready(() => {

    document.getElementById("weathercam").src = imagePath+'/latest.jpg'
});


