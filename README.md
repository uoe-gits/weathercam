# weathercam
Utilities to collect images from the School of GeoSciences weather camera.

## Programs
 
 * `weathercam-download` is used to download images from the weather cam. It  can be configured with a configuration file. By default it will only download a single image. Run it with the `-i` option to set a download interval in minutes.
 
 * `weathercam-scale` can be used to copy and scale the recent set of images. It uses the time stamps of the images to figure out the oldest file to overwrite. You do not usually need to run this program as the functionality is integrated into `weathercam-download` when the configuration file contains a `scale` section.
 
 * `weathercam-create` is a wrapper around `ffmpeg` and can be used to create an animation of today's captured images. If the program is run during the first hour of a day it will create an animation from the images of the previous day. You can specify the number of animations to keep. In that case it also produces a json file which lists the current files.
 
As always use the `-h` or `--help` flag to get an overview of the command line options.

## Configuration
The tools take an inistyle configuration file.

	[weathercam]
	# the URL to where the image can be downloaded from
	url = http://some.camer.org/current_image
	# the username
	username = user
	# and password
	password = 123password

	[output]
	# controls whether images are put into a deep directory structure
	# of the form of YYYY/YYYY-MM-DD/HH/
	deep = True
	# directory to write images to, if not set write to present working
	# directory
	path = /some/directory
	# the type of image to write
	suffix = .jpg
	# you can enhance the contrast by setting this factor
	# somewhere between 1 and 2 might be useful
	contrast = 1
	# add a timestamp to the image
	timestamp = True
	
	[scale]
	# write scaled version of the images to a separate path
	path = scaled
	# the width of the scaled image
	width = 800
	# keep this number of scaled images
	num_images = 60
	# use this number of digits in the output file name
	ndigit = 3
	
The secret username and password can be stored in a second config file which will get merged when the program runs.

The program will prodcue scaled versions of the images if the `scale` section is present in the configuration file. Only a fixed number of files are kept (by default 60). File names are formed of the image index.
