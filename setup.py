from setuptools import setup, find_packages

setup(
    name = "weathercam",
    packages = find_packages(),
    install_requires = [
        "requests",
        "Pillow",
        "grandfatherson",
    ],
    entry_points={
        'console_scripts': [
            'weathercam-download = weathercam.download:main',
            'weathercam-create = weathercam.createmov:main',
            'weathercam-scale = weathercam.scale:main',
            'weathercam-cull = weathercam.cull:main',
        ],
    },
    author = "Magnus Hagdorn",
    description = "handle images from weathercam",
)
