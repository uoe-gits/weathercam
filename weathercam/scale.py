import argparse
from pathlib import Path
import shutil
from PIL import Image
import logging

from .log import logOpts, initLog

def scale(image,outdir,numimages,ndigit=3,width=800):
    fmt='{0:0%dd}.jpg'%ndigit
    idx=0
    try:
        newest = max(outdir.glob('[0-9]*.jpg'),
                     key=lambda img: img.stat().st_mtime)
    except ValueError:
        newest = None
    if newest is not None:
        if image.stat().st_mtime<=newest.stat().st_mtime:
            logging.debug(f'{image} is older than {newest}')
            return
        idx = int(newest.stem)+1
        if idx==numimages:
            idx = 0

    logging.debug(f'scaling {image}')
    img = Image.open(image)
    factor = width/img.size[0]
    newSize = (int(img.size[0]*factor),int(img.size[1]*factor))
    img = img.resize(newSize, Image.ANTIALIAS)
    outname = outdir/fmt.format(idx)
    if not outdir.exists():
        logging.debug(f'creating {outdir}')
        outdir.mkdir(parents=True)
    logging.debug(f'writing {outname}')
    img.save(outname)
    shutil.copystat(image,outname)
    # create symbolic link
    latest = Path(outdir,'latest.jpg')
    if latest.exists():
        latest.unlink()
    latest.symlink_to(fmt.format(idx))
    # write current index into json file
    with open(outdir/'idx.json','w') as idxjson:
        idxjson.write(f'{{"idx":{idx}}}')


def main():
    parser = argparse.ArgumentParser(parents=[logOpts()])
    parser.add_argument('input',type=Path,
                        help="directory tree containing images")
    parser.add_argument('-o','--output',default=Path("scaled"),
                        type=Path, help="name of output directory")
    parser.add_argument('-n','--num-images',type=int,default=60,
                        help="the number of images to keep")
    args = parser.parse_args()
    initLog(args)
    
    images = sorted(args.input.glob("**/*.jpg"),
                    key=lambda img: img.stat().st_mtime)

    start = max(len(images)-args.num_images,0)
    for img in images[start:]:
        scale(img,args.output,args.num_images)

    
if __name__ == '__main__':
    main()
