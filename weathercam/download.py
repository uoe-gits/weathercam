import argparse
import configparser
import requests
from PIL import Image, ImageFont, ImageDraw, ImageEnhance
from datetime import datetime
from pathlib import Path
import pytz
import signal
import sys
import time
import logging

from .log import logOpts, initLog
from .scale import scale

def signal_handler(sig, frame):
    logging.info('received SIGINT')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

class WeatherCamImg:
    def __init__(self, url, username=None, password=None):
        self._url = url
        logging.info(f'downloading images from {self._url}')
        if username is None or password is None:
            self._auth = None
        else:
            self._auth = (username, password)

        self._font = "FreeMono.ttf"
        self._font_size = 24
        self._colour = (255,255,255)
        
        self._ts = None
        self._img = None
        self._fname = None

    @property
    def font(self):
        return ImageFont.truetype(self._font, self._font_size)

    @property
    def fname(self):
        return self._fname

    def get(self):
        logging.debug(f'getting image from {self._url}')
        response = requests.get(self._url, auth=self._auth, stream=True)
        self._ts = datetime.now(tz=pytz.UTC)
        if response.status_code != 200:
            raise RuntimeError(f'{response.status_code}: {response.reason}')
        self._img = Image.open(response.raw)

    def contrast(self,factor=1.2):
        if factor != 1:
            logging.debug('enhancing contrast')
            enhancer = ImageEnhance.Contrast(self._img)
            self._img = enhancer.enhance(factor)
        
    def addTimeStamp(self):
        logging.debug('adding time stamp')
        draw = ImageDraw.Draw(self._img)
        draw.text((50, self._img.size[1]-50), self._ts.strftime('%Y-%m-%d %H:%M:%S'),
                  self._colour, font=self.font)

    def save(self,path=None, deep=True, suffix='.jpg'):
        fname = Path(self._ts.strftime('%Y%m%d_%H%M%S'))
        if deep:
            fname = Path(self._ts.strftime('%Y'),
                         self._ts.strftime('%Y-%m-%d'),
                         self._ts.strftime('%H'))/fname
        if path is not None:
            fname = Path(path)/fname
        if not fname.parent.exists():
            fname.parent.mkdir(parents=True)
        self._fname = fname.with_suffix(suffix)
        logging.debug(f'saving {self.fname}')
        self._img.save(self.fname)
        
def main():
    parser = argparse.ArgumentParser(parents=[logOpts()])
    parser.add_argument('config', help="name of configuration file")
    parser.add_argument('-s','--secrets',help="load secrets from separate file")
    parser.add_argument('-i','--interval',metavar='MIN', type=int,
                        help="download image every MIN minutes,"
                        " default only download single image")
    args = parser.parse_args()
    initLog(args)

    cfg = configparser.ConfigParser()
    cfg.read(args.config)
    if args.secrets is not None:
        cfg.read(args.secrets)

    path = None
    if 'path' in cfg['output']:
        path = Path(cfg['output']['path'])
    deep = True
    if 'deep' in cfg['output']:
        deep = cfg['output'].getboolean('deep')
    suffix = '.jpg'
    if 'suffix' in cfg['output']:
        suffix = cfg['output']['suffix']

    # setup scale configuration
    scalecfg = None
    if 'scale' in cfg:
        scalepath = "scaled"
        if "path" in cfg["scale"]:
            scalepath = Path(cfg["scale"]["path"])
        numImages = 60
        if "num_images" in cfg["scale"]:
            numImages = cfg["scale"].getint("num_images")
        scalecfg = {"width":800, "ndigit":3}
        for o in ["width","ndigit"]:
            if o in cfg["scale"]:
                scalecfg[o] = cfg["scale"].getint(o)

    wcImg = WeatherCamImg(cfg['weathercam']['url'],
                          username = cfg['weathercam']['username'],
                          password = cfg['weathercam']['password'])
    while True:
        try:
            wcImg.get()
        
            if 'contrast' in cfg['output']:
                wcImg.contrast(factor=cfg['output'].getfloat('contrast'))
            if 'timestamp' in cfg['output'] and \
               cfg['output'].getboolean('timestamp'):
                wcImg.addTimeStamp()

            wcImg.save(path=path, deep=deep, suffix=suffix)

            # scale image
            if scalecfg is not None:
                scale(wcImg.fname, scalepath, numImages, **scalecfg)

        except Exception as e:
            logging.error(e)

        ts = datetime.now()
        
        if args.interval is None:
            break
        else:
            # wait until next full minute
            nsec = args.interval*60-(ts.second+ts.microsecond*1e-6)
            logging.debug(f'waiting {nsec} until downloading next image')
            time.sleep(nsec)
            
if __name__ == '__main__':
    main()
