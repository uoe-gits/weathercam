import os
import argparse
import datetime
import subprocess
from pathlib import Path
from grandfatherson import to_delete

def get_timestamp_from_filename(filename):
    # Files are named 'yyyymmdd_HHMMSS.jpg'
    #timestamp = datetime.datetime.fromtimestamp(
    #    os.path.getmtime(fname)
    #)
    fullfile = filename.split('.')[0]
    return datetime.datetime.strptime(fullfile, '%Y%m%d_%H%M%S')

def get_images(top_dir):
    # Returns a list of (filename, timestamp) tuples of all jpg files 
    # in subdirectories of top_dir
    result = []
    for root, subdirs, files in os.walk(top_dir):
        for f in files:
            if not f.endswith('.jpg'):
                continue
            fname = os.path.join(root, f)
            timestamp = get_timestamp_from_filename(f)
            result.append((fname, timestamp))
    return result

def main(args):
    # Calculate the number of per minute and hourly photos to keep
    MINUTES_TO_KEEP = 60 * 24 * 7 # Keep per minute photos for a week (10080)
    HOURS_TO_KEEP = 365 * 24 # Keep per hour photos for a year (8760)

    # Get the files, timestamps and mapping from timestamp to filename
    files = get_images(args.im_dir)
    times = [f[1] for f in files]
    file_lookup = {ts: f for f, ts in files}

    # Get the timestamps of unwanted files.
    times_to_delete = to_delete(
        times, 
        hours=HOURS_TO_KEEP, 
        minutes=MINUTES_TO_KEEP,
    )

    if args.X:
        # Delete files, with a last chance for escape.
        print(f'About to delete {len(times_to_delete)} images [y/N]')
        yes_no = input()
        if not yes_no.lower() in ['y', 'yes']:
            print('abort')
            return

        for ts in times_to_delete:
            os.remove(file_lookup[ts])

        # Delete empty directories using 'find <args.im_dir> -type d -empty -delete'
        find_cmd = [
            'find',
            args.im_dir,
            '-type', 'd',
            '-empty',
            '-delete',
        ]
        subprocess.call(find_cmd)

    else:
        # Print the files marked for deletion.
        for ts in sorted(times_to_delete):
            print(ts, file_lookup[ts])

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Remove old images')
    parser.add_argument('-X', action='store_true', default=False,
                        help='Delete old images')
    parser.add_argument('im_dir', type=Path,
                        help='Top level directory containing jpeg images')
    args = parser.parse_args()

    main(args)
    
