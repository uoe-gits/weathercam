import argparse
from pathlib import Path
from datetime import datetime, timedelta
from PIL import Image
import subprocess
import json
import logging

from .log import logOpts, initLog

def main():
    parser = argparse.ArgumentParser(parents=[logOpts()])
    parser.add_argument('input',type=Path,
                        help="directory tree containing images")
    parser.add_argument('-o','--output',default=Path("."),
                        type=Path, help="name of output directory")
    parser.add_argument('-f','--framerate',default=30,
                        type=int, help="framerate, default 30")
    parser.add_argument('-d','--date', metavar="YYYY-MM-DD",
                        help="specify date for which to create"
                        "timelapse animation, default: today")
    parser.add_argument('-k','--keep-num',metavar="N", type=int,
                        help="keep N files, by default keep all")

    args = parser.parse_args()
    initLog(args)

    if args.date is not None:
        date = datetime.strptime(args.date, '%Y-%m-%d')
    else:
        now = datetime.now()
        date = datetime(now.year, now.month, now.day)
        if (now-date).seconds < 3600:
            # we are running the program during the first hour of the day, so let's
            # create the full animation of the previous day
            date = date - timedelta(1)

    logging.info(f'creating animation for {date.date()}')

    inpath = args.input/Path(date.strftime('%Y'),
                             date.strftime('%Y-%m-%d'))
    glob = Path('*','*.jpg')

    # get an image to figure out resolution
    try:
        img = Image.open(next(inpath.glob(str(glob))))
    except:
        parser.error(f'could not open image from {inpath/glob}')
    
    ffmpeg_cmd = [
        '/usr/bin/ffmpeg',
        '-framerate', str(args.framerate),
        '-pattern_type', 'glob', '-i', str(inpath/glob),
        '-s:v', '{0}x{1}'.format(*img.size),
        '-c:v', 'libx264', '-crf', '17', '-pix_fmt', 'yuv420p']
    if args.log_level != 'debug':
        ffmpeg_cmd += ['-loglevel','quiet','-hide_banner']
    outfile = args.output/Path(date.strftime('%Y-%m-%d')).with_suffix('.mp4')
    if outfile.exists():
        outfile.unlink()
    ffmpeg_cmd.append(str(outfile))
    logging.info(f'writing {outfile}')

    logging.debug(' '.join(ffmpeg_cmd))
    subprocess.call(ffmpeg_cmd)

    if args.keep_num is not None:
        animations = sorted(args.output.glob('*.mp4'))
        while len(animations) > args.keep_num:
            a = animations.pop(0)
            logging.info(f'removing old file {a}')
            a.unlink()
        output = {'animations':[]}
        for a in animations:
            output['animations'].append(a.name)
        with open(args.output/'animations.json','w') as out:
            json.dump(output,out)
    
if __name__ == '__main__':
    main()
